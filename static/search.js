function debounce(func, timeout = 300){
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}

const index = elasticlunr.Index.load(window.searchIndex);
const input = document.getElementById("search");
const output = document.getElementById("results");

input.addEventListener("keyup", debounce(() => search()));
input.addEventListener("focusout", debounce(() => clear()));
input.addEventListener("focusin", search);

const options = {
    bool: "AND",
    fields: {
        title: {boost: 2},
        body: {boost: 1}
    }
};

function clear() {
    output.style.visibility = "hidden";

    while (output.firstChild) {
        output.removeChild(output.firstChild);
    }
}

function search() {
    clear();

    const results = index.search(input.value, options);
    [...results].forEach(result => {
        var title = document.createElement("h3");
        title.appendChild(document.createTextNode(result.doc.title));

        var p = document.createElement("p");
        p.appendChild(document.createTextNode(result.doc.body));

        var a = document.createElement("a");
        a.setAttribute("href", result.doc.id);
        a.appendChild(title);

        // var r = document.createElement("a");
        // r.setAttribute("href", result.doc.id);
        // r.setAttribute("class", "read-more");
        // r.appendChild(document.createTextNode("Lire plus"));

        var section = document.createElement("section");
        section.setAttribute("class", "search-result");

        section.appendChild(a);
        section.appendChild(p);
        // section.appendChild(r);

        var li = document.createElement("li");
        li.appendChild(section);

        output.appendChild(li);
    });

    if (output.firstChild) {
        output.style.visibility = "visible";
    }
}
