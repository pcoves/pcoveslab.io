+++
title = "Accueil"
description = "Qu'il est bon de rentrer au nid"

sort_by = "weight"

[extra.hero]
title = "Autruche"
description = """
Les autruches sont tellement cools !

C'est vrai quoi, il suffit de les regarder pour s'en rendre compte.
Leur cerveau est plus petit que leurs yeux...
Et pourtant, elles sont là, [dinosaures](https://fr.wikipedia.org/wiki/Ornithomimosauria) ~~marchant~~ courant parmis nous.

**Le savais-tu** : elles ne se cachent pas en enfonçant leur tête dans le sable.
En fait, elles s'occupent de leurs œufs enfouis, d'où, la légende.
"""
+++
