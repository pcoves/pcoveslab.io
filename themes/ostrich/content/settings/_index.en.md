+++
title = ":gear:"
description = "Site wide settings"

weight = 0
template = "settings.html"

[extra]
noscript = true
+++
