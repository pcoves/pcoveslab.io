+++
title = "Index"
description = "There's no place like 127.0.0.1"

sort_by = "weight"

[extra.hero]
title = "Hello there :wave:"
description = """
And welcome.

I've been a software developper for the last ten years.
And I often told myself I should write about my days.

You'll find my latests posts in the [blog](/blog) section.
Don't miss the [French](/) version of the whole website :ok_hand:

If you ever look for someone to hire, I'm open to work!
I'm curious about everything and love to share what I find interesting.
Have a look at my [CV](https://gitlab.com/api/v4/projects/34610672/packages/generic/cv/latest/cvPabloCOVES.en.pdf).
And don't hesitate reach out!
"""
+++
