+++
title = "Accueil"
description = "Comme à la maison"

sort_by = "weight"

[extra.hero]
title = "Hello there :wave:"
description = """
Et bienvenue sur mon site.

Développeur depuis 10 ans, cela fait plusieurs années que j'éprouve le besoin de partager ma veille technologique.

Tu trouveras donc sur ce site mes recherches détaillées dans mes articles de [blog](/blog).
Il y a aussi la version [anglophone](/en) du site à visiter :ok_hand:

Et si tu cherches à embaucher un programmeur curieux touche-à-tout, n'hésite pas à visiter mon [CV](https://gitlab.com/api/v4/projects/34610672/packages/generic/cv/latest/cvPabloCOVES.pdf) et à me contacter par mail !
"""
+++
