+++
title = "Projets"
description = "Parce que j'aime bien parler de ce que je fais à qui veut l'entendre mais que le format blog n'est pas toujours adapté"

weight = 2

sort_by = "weight"
+++

La plupart des développeurs ont de petits (ou gros hein) projets qu'ils codent sur leur temps libre.
Et bien je ne fais pas exception à cette rêgle.

C'est ici que je viens en parler.
Sans prétention aucune, ces projets sont généralement sous licence `MIT` ou `WTFPL`.
Ne pas hésiter donc à piocher dans tout ou une partie du code!
