+++
title = ":gear:"
description = "Site wide settings"

weight = 99
template = "settings.html"

[extra]
noscript = true
+++
