+++
title = "Merge requests automatiques"
description = "Ou comment ouvrir une merge request via l'API gitlab et gagner du temps sur le clicodrome"

[taxonomies]
sujets = ["Git", "gitlab", "merge request", "automatisation"]

[extra]
thumbnail = "gitlab.svg"
+++

Que ce soit à titer personnel ou pour le travail, j'utilise `gitlab`.
Sur la plupart des *side-projects*, j'œuvre seul mais le reste du temps, il est normal de passer par des *merge requests* afin de lancer les tests automatiques et permettre une revue de code en bonne et due forme.
Seulement voilà: comme beaucoup, je n'aime pas le clicodrome.

Par chance, `gitlab` propose des [options](https://docs.gitlab.com/ee/user/project/push_options.html#push-options-for-merge-requests) disponnible au *push* pour créer une *merge request*.
Cet article présente un petit script qui m'est bien pratique au quotidien pour ne pas passer trop de temps sur ma souris.

<!-- more -->

## Le code

```sh
#!/bin/sh

branch=$(git symbolic-ref --short HEAD)

function help {
    echo "Usage"
    echo
    echo "$0 [options] [-v]"
    echo
    echo "Options:"
    echo "  -t title         ( defaults to \"Draft: $branch\")"
    echo "  -d description   ( defaults to \"$branch\")"
    echo "  -b target branch ( defaults to \"master\")"
    echo "  -v verbose"
}

while getopts ":hvt:d:b:" option; do
    case $option in
        h)
            help
            exit;;
        \?)
            echo Invalid -$OPTARG option
            help
            exit;;
        v) verbose=1;;
        t) title=$OPTARG;;
        d) description=$OPTARG;;
        b) target=$OPTARG;;
    esac
done

title=${title:-"Draft: $branch"}
description=${description:-"$branch"}
target=${target:-"master"}

if [ $verbose ]
then
    echo "Title       would be \"$title\""
    echo "Description would be \"$description\""
    echo "Target      would be \"$target\""
    exit
fi

git push -u origin $branch \
    -o merge_request.create \
    -o merge_request.target="$target" \
    -o merge_request.merge_when_pipeline_succeeds \
    -o merge_request.remove_source_branch \
    -o merge_request.title="$title" \
    -o merge_request.description="$description"
```

## L'usage

Le script suivant est à placer dans `~/.local/bin/mr` et à rendre exécutable `chmod +x ~/.local/bin/mr`.

```
❯ mr -h
Usage

/home/pcoves/.local/bin/mr [options] [-v]

Options:
  -t title         ( defaults to "Draft: $nom_de_la_branche_courante")
  -d description   ( defaults to "$nom_de_la_brache_courante")
  -b target branch ( defaults to "master")
  -v verbose
```

Autrement dit `mr -t Tourbilol -d Autruche -b Foobar` créera une *merge request* nommée `Tourbilol` avec `Autruche` en description et pour branche cible `Foobar`.

À noter que, sur `gitlab`, la présence du préfixe `Draft: ` dans le titre de la *merge request* permet d'indiquer au collègue que le code n'est pas prêt à être passé au crible d'une revue.
S'il y est par défaut, il ne faut pas oublier de le mettre manuellement lors de l'usage du *flag* `-t`.

## Amélioration

Une amélioration à laquelle je pense maintenant serait de récupérer la branche par défaut du dépôt courant au lieu de viser `master`.
De nos jours, on nombre de dépôts ont changé `master` pour `main` et il n'est pas rare que je préfère viser `alpha` dans mon quotidien pour tester au lieu de directement déployer en production.
