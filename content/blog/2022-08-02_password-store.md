+++
title = "Password-store, le retour"
description = "Ou comment avoir plusieurs identités pour un seul gestionnaire de mots de passe"

draft = false

[taxonomies]
sujets = ["IT", "Password-store", "Pass", "Smart-phone", "Android"]

[extra]
thumbnail = "pass.png"
+++

J'ai réinstallé une ROM Android neuve sur mon téléphone il y a quelques jours.
Ce faisant, j'ai effacé intégralement les données enregistrées, incluant mon gestionnaire de mots de passe [`password-store`][pass] (`pass` pour les intimes).

Cet article explique la marche à suivre pour récupérer mes informations chiffrées sur une machine fraîchement installée.
Il fait suite à [pass](/blog/pass) et repose aussi sur [gpg-pgp](/blog/gpg-pgp).

<!-- more -->

# Prérequis

Tout d'abord, côté PC: `password-store` bien sûr et `gpg` pour le chiffrement avec le dépôt `git` à jour.
Le minimum fonctionnel en réalité, pas de surprise.

Sur le téléphone "neuf", il faut installer `Password Store` et `OpenKeychain`.
Ces deux applications sont accessibles via `F-Droid`.

# Préparation

## Téléphone

C'est sur la nouvelle machine que commence le travail.
L'idée, même si elle m'appartient, est de la considérer comme un nouvel utilisateur gagnant accès à mon *vault* de mots de passe.
Il faut donc créer un nouveau trousseau de clés via `OpenKeychain` et en partager la partie publique avec l'ordinateur.

### Création d'une nouvelle clé

Par défaut, `OpenKeychain` génère des clés pour la *certification*, la *signature* et le *chiffrement*.
Étant donné que les mots de passe sont synchronisés via `git`, il faut aussi ajouter une sous-clé pour l'authentification.

Ne pas oublier d'y ajouter un mot de passe en guise de *master password*.

### Partage des clés publiques

Une fois fait, deux partages:
* La clé `SSH` doit être poussée sur la plateforme fournissant `git`,
* Les clés publiques doivent être partagées sur l'ordinateur.

## Ordinateur

Une fois le fichier contenant la nouvelle clé récupérée sur la machine, quelques étapes à suivre.

### Import de la nouvelle clé

Un simple `gpg --import $fichier` suffit à ajouter l'identité du téléphone sur l'ordinateur.

### Réglage de la confiance

`Password-store` demande à ce que les clés utilisées pour le partage de mots de passe aient le niveau de confiance le plus élevé: `5/5`.
Il faut donc éditer la clé précédemment ajoutée pour modifier cette information à son propos:

```bash
$ gpg --edit-key $key
trust 5
y
save
```

### Ajout de l'utilisateur

Cette partie est déjà traitée dans [`pass`](blog/pass/#modifier-ajouter-un-membre) mais mérite un peu plus de détail cette fois-ci.

* Dans un premier temps il faut ajouter l'emprunte (*fingerprint*) de la clé dans le fichier `~/.password-store/.gpg-id`.
* Ensuite, la commande "magique" `pass init $(cat ~/.password-store/.gpg-id)`.

Cette dernière commande va générer deux *commits*.
Le premier indique l'altération des *ID* `GPG` utilisés.
Le second correspond à `password-store` qui se sera chargé de (re)chiffrer tous les mots de passe avec la nouvelle clé.

# Synchronisation

À ce moment, le téléphone ne détient que la paire de clés publiques/privées.
L'ordinateur quant à lui contient la clé publique et tous les mots de passe chiffrés pour toutes les clés sur le dépôt `git` synchronisé.
Il est temps de récupérer ce dernier sur le téléphone.

Pour ce faire, il faut lancer `Password Store` pour Android et se laisser guider dans la récupération d'un dépôt distant.
L'adresse est de la forme `git@$fournisseur:chemin/depot` et il faut choisir d'utiliser `OpenKeychain` avec la clé récemment générée.

L'application va demander un mot de passe pour accéder au dépôt.
C'est celui du compte en temps normal ou un *token* d'accès personnel si le *2FA* a été activé.

# Notes

* Cette approche permet de ne jamais faire sortir les clés privées des machines sur lesquelles elles ont été générées.
* Un ordinateur est nécessaire pour suivre cette démarche. Il n'est pas possible de tout faire depuis le téléphone.

[pass]: https://www.passwordstore.org/
