+++
title = "Some thoughts about NVim"
description = "A personnal take on the future of NVim core"

[taxonomies]
about = ["NVim", "futur", "of", "software"]

[extra]
thumbnail = "neovim.svg"
+++

First, there's been [this blog post][phaazon] by Dimitri Sabadie aka Phaazon.
A very interesting reading.
Notably given the fact he's a plugin author but also the mind behind [This Week in Neovim][twin].
The guy knows what he talks about.
And his take is that [`nvim`][nvim] is getting too big, almost bloated already.

Then [this Reddit post][reddit] about both Phaazon's write-up and a previous poll about moving features into [`nvim`][nvim]'s core.
The author, [u/premell][premell] states that having too many plugins leads to unstable setups.
One plugin could bring breaking changes to its API. And other plugins relying on it could stop working as expected.
As far as I understand, he thinks that some wildly used plugins should make their way to [`nvim`][nvim]'s core to guarantee stability and prosperity to others.

I'm not sure it's the right way of doing things.

<!-- more -->

## Core maintainers VS Plugin authors

First of all, I think [`nvim`][nvim] should be easy to maintain.
It's a huge task to keep such software stable and reliable for others.
Maintainers should, IMO, strive to keep things as stable, reliable, and easy to reason about as possible.
Not only this will ease their work but it'll also make it easier for newcomers to embrace the codebase.

Once a piece of code goes into the core, there is virtually no way to remove it.
Other core features as well as plugins authors will rely on it and it'll have to stay for good in [`nvim`][nvim].
From here, I see three possibilities:
1. Everything goes right. The feature is rock solid and was perfectly designed from the ground up.
2. It goes okay-ish. The feature needs some adjustments and they are merged in the next [`nvim`][nvim]'s release. Hence, core maintainers work instead of plugin authors' work.
3. It goes wrong. Maybe not in the first year or so. But a time comes when the feature is not easily maintainable anymore. But it has to be. Hence, a lot of repetitive core maintainers work.

If a plugin, even one used by more than 90% of people spawning [`nvim`][nvim] daily, can keep being a plugin, I think it should stay that way.

Suffice it to say some plugins are known for their usefulness, stability, and number of users.
[Tim Pope][tpope] wonderful work is proof of that.
And, yes, some features of some plugins made their way to the core.
I'm not saying this should **never** happen.
But [`nvim`][nvim] is still young compared to [`vim`][vim] and so is its `lua` ecosystem.
I'd rather use code as a plugin for ten more years than see it in the core next year.
The former relies on its author's work which sounds right to me.
While the latter becomes the core maintainers' burden.
And I'm sure they have better things to do in their free time.

## Old farts VS Newcomers

Then there is this discussion about whether should [`nvim`][nvim] give newcomers what they expect right away or not.

Sure, if you come from an IDE of any sort, you'd expect some features to be there out of the box.
A side tray with a file explorer is an often-cited example.
And to be fair, when I started using [`vim`][vim] more than a decade ago, it seemed nut to me that I had to define `makeprg` by hand instead of pressing whatever `F` key I was used to.

So what?
This is [`vim`][vim], this is [`nvim`][nvim] and I'm pretty sure this stands for [`emacs`][emacs] as well.
One does not technically **need** a nicely-disposed file explorer to develop features.
One does not even **need** LSP to do such a thing.
I've met many people who still don't use them and still get the work done on time.

Why should core maintainers integrate features that are not 100% mandatory for text manipulation?
It's a text editor after all.
An extendable, hackable one.
But a text editor nonetheless.
I think it should stay that way and embed the bare minimum required to edit text *the vim way* and load plugins.

## Avoid success at all costs

I first met this mantra when learning [Haskell][haskell].
And it took me more time than I'm willing to admit to getting it right.

At first, I read it as *"Avoid success" at all costs*.
Why would anyone create a programming language or software and then want it to fail?
Made no sense.

But no, the right way to read it is *Avoid "success at all costs "*.
[`Vim`][Vim], [`nvim`][nvim], [`emacs`][emacs] and the like are not made for everyone.
They're designed so anyone can make them their own.
But it requires work.

Don't get me wrong here.
I'm not saying that one should find happiness through suffering.
My point is, giving people what they're used to in other ecosystems should not be [`nvim`][nvim] community goal.
Yes, it'll drive people away.
It'll make some complain and write some flaming comments on the internet because they were not able to leave the editor.
And that's okay.
Because it'll also retain users willing to understand and embrace the [`vim`][vim] way of doing things.
And I'd much rather keep [`nvim`][nvim] this way than see it turn into a fully-fledged IDE by default.

## Conclusion

This maybe is the first post about a personal viewpoint.
I usually write about how/why I use something and not what I think about what people think.

I hope you liked it anyway.
You can tell me how right or wrong I am on [Mastodon][mastodon] if you want.

In either case: happy hacking in whatever software makes you happy!

[phaazon]: https://phaazon.net/blog/editors-in-2022
[twin]: https://this-week-in-neovim.org/
[reddit]: https://www.reddit.com/r/neovim/comments/101omgs/discussion_about_the_state_of_neovims_plugin/
[premell]: https://www.reddit.com/user/premell/
[nvim]: https://neovim.io/
[tpope]: https://github.com/tpope
[haskell]: https://www.haskell.org/
[mastodon]: https://mamot.fr/@PacoVelobs
[nvim]: https://neovim.io/
[vim]: https://www.vim.org/
[emacs]: https://www.gnu.org/software/emacs/
