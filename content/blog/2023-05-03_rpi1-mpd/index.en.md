+++
title = "Repurpose a first generation Raspberry Pi"
description = "Create a web-radio with an old Raspberry Pi"

[taxonomies]
about = ["Raspberry Pi", "Web-radio", "Raspbian", "MPD"]
+++

I stumbled upon my first-generation Raspberry Pi yesterday.
And told myself I should do something with it.

Also, I spend most of my days listening to music.
But I don't like the idea of paying for whatever fancy streaming service.
So, yeah, here is how to make your custom web-radio with on old computer and a couple of commands.

<!-- more -->

## Raspbian

While I'm a fierce defender of Arch Linux as a general purpose operating system, I think it's best to stick to basic for such a project.
Also, Arch is a pain to install on a Pi, and it'd not bring any advantage over Raspbian here.

I'm not going to explain how to install Raspbian on a Pi in this post.
Every information is to be found [here](https://www.raspberrypi.com/software/).
It takes half a dozen of clicks to make it right with the GUI tool.

No need for a fully-fledged desktop environment here.
Pick the `Raspberry Pi OS Light (32-bits)` under the `Raspberry Pi OS (other)` menu.

### Notes

I'm using a venerable Raspberry Pi 1 here.
So, no Wi-Fi, no Bluetooth, only a reliable `ssh` connection over good old Ethernet wires.

The installation tool allows creating a user with password and a `ssh` public key.
I chose this option in order to avoid plugging it to a screen.

## Setup

Once the system is on the SD card, it's time to power it on.

For once, I was happy to pock around **the box**.
You know, **the box** of wires you keep *in case you need it* but never do?
Well, today I did.

USB in one port, RJ45 in the other.
Both plugged right into the ISP's hardware.
The Raspberry Pi 1 does not need much power to run so no need for a dedicated power source.

### Software installation

```bash
apt update && apt upgrade -y
apt install -y mpd ncmpc
```

Basically, update the system and install :
* `mpd` to play the music,
* `ncmpc` to control `mpd`.

`Ncmpc` is a `ncurse` interface over `mpc`.
I find it much more enjoyable than `mpc` as it comes with embedded help and better music library browsing.
But in the end, it does the same thing and won't be used much.

### Notes

At the end of the installation process there are those two lines :

```
Created symlink /etc/systemd/system/multi-user.target.wants/mpd.service → /lib/systemd/system/mpd.service.
Created symlink /etc/systemd/system/sockets.target.wants/mpd.socket → /lib/systemd/system/mpd.socket.
```

Awesome, `mpd` will start on boot!
And it's already running :

```
root@rpi1:~# systemctl status mpd.service
● mpd.service - Music Player Daemon
     Loaded: loaded (/lib/systemd/system/mpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2023-05-03 10:08:54 CEST; 15min ago
TriggeredBy: ● mpd.socket
       Docs: man:mpd(1)
             man:mpd.conf(5)
             file:///usr/share/doc/mpd/html/user.html
   Main PID: 5221 (mpd)
      Tasks: 3 (limit: 415)
        CPU: 8.037s
     CGroup: /system.slice/mpd.service
             └─5221 /usr/bin/mpd --no-daemon

May 03 10:08:44 rpi1 systemd[1]: Starting Music Player Daemon...
May 03 10:08:53 rpi1 mpd[5221]: May 03 10:08 : exception: Failed to open '/var/lib/mpd/tag_cache': No such file or directory
May 03 10:08:54 rpi1 mpd[5221]: May 03 10:08 : output: No 'audio_output' defined in config file
May 03 10:08:54 rpi1 mpd[5221]: May 03 10:08 : output: Successfully detected a sndio audio device
May 03 10:08:54 rpi1 systemd[1]: Started Music Player Daemon.
```

It's running but with a pair of messages :

```
May 03 10:08:53 rpi1 mpd[5221]: May 03 10:08 : exception: Failed to open '/var/lib/mpd/tag_cache': No such file or directory
May 03 10:08:54 rpi1 mpd[5221]: May 03 10:08 : output: No 'audio_output' defined in config file
May 03 10:08:54 rpi1 mpd[5221]: May 03 10:08 : output: Successfully detected a sndio audio device
```

The first line is here only because the `tag_cache` did not exist before.
And `mpd` created it on first launch, so it's not a real issue.

Time to fix the other two.

### Software configuration

The configuration for `mpd` is at `/etc/mpd.conf`.

{% note(class="warning") %}
First thing first, make a copy of the original file in case you mess things up:

```bash
cp /etc/mpd.conf /etc/mpd.save.conf
```
{% end %}

I'm not going into details over each line as most are perfectly sane by default.
My goal here is to fix the warnings seen above and have the web-radio work as intended.

```conf,hl_lines=1 11
music_directory    "/var/lib/mpd/music"
playlist_directory "/var/lib/mpd/playlists"
db_file            "/var/lib/mpd/tag_cache"
log_file           "/var/log/mpd/mpd.log"
pid_file           "/run/mpd/pid"
state_file         "/var/lib/mpd/state"
sticker_file       "/var/lib/mpd/sticker.sql"

user            "mpd"
bind_to_address "localhost"
#password       "${fancy-password}@read,add,control,admin"

input {
        plugin "curl"
}

decoder {
        plugin        "wildmidi"
        enabled       "no"
}

audio_output {
        type            "httpd"
        name            "My HTTP Stream"
        encoder         "vorbis"
        port            "8000"
        bind_to_address "0.0.0.0"
        bitrate         "128"
        format          "44100:16:1"
        max_clients     "0"
}
```

That's enough :
* Files are at `/var/lib/mpd/music`. You can pick another path but be sure to create the directory before restarting `mpd`,
* Pick a password and uncomment this line. More on this later.

### Give it a try

1. First, put some files in the music directory.
2. Then restart `mpd` : `systemctl restart mpd`.
3. Fires up `ncpmd`, enter your password and press `Ctrl-U` to update `mpd`'s library.
4. Press `3` to go to the said library.

If all goes according to the plan, you should see the sounds you copied.
Else, well, *it works on my machine ®* so, not sure about what went wrong on your side.

1. Add some sound with `a`.
2. Press `2` to go to the sounds queue.
3. Press `Enter` to play a sound.

This should start playing some music.
To ~~see~~ hear if it works as intended, open a new tab in your favorite web-browser and navigate to `${RPi-IP}:8000`.
You can get the IP address with the `ifconfig` command.

Here, I can hear the music coming from my Raspberry Pi to my laptop : I now have a local web-radio :tada:

## Remote control

I'm pretty sure nobody wants to open a terminal to manage its playlists in 2023.
And, be amazed, it's possible to do it from your very smartphone!

The [MPDroid](https://f-droid.org/packages/com.namelessdev.mpdroid/) is a nice little asset in our pocket.
It's not been updated for years, but it does the trick nicely!

{% note(class="warning") %}
Be sure to connect your smartphone to the same network as the Raspberry Pi !
{% end %}

## Bonus

### Make it big

I don't know about you, but my music library can't fit in a 16Go SD card.

By chance, I also have a much bigger hard-drive lying around.
And solving the SD space issue is pretty easy :
* Plug the hard-drive and fetch its `UUID` or `PARTUUID`.
* Use this information to auto-mount the HD on boot by editing `/etc/fstab`.
* Update `/etc/mpd.conf` to use it as it's new `music_directory`.

Done, you have more space for your music.

### Make it wide

Okay, there is the *web-radio*.
But it's a *local web-radio* : it does not go outside your flat.

The trick here is to first give you Raspberry Pi a static local IP and then enable ports `6600` and `8000` redirection to it.
I can't give any advice on how to do it as it depends on your ISP and hardware.

Once done, suffice it to change the local Raspberry Pi's IP address to your ISP's box public IP.
And, here you go, public facing *web-radio* for the win!

## Conclusion

It only takes a few commands and a pair of settings to be able to stream your favorite music.

Depending on where you live it might be legal or not.
I wish there would be a way to add password authentication on the `8000` port, but I'm not sure if it's really feasible as is.

Anyway, time to copy my music library to the Raspberry Pi :metal:
