+++
title = "Die CAPSLOCK, Die!"
description = "Capslock, réminiscence des temps anciens, Balrog des claviers, ne devrait plus être"

[taxonomies]
sujets = ["IT", "CapsLock", "Escape", "Clavier", "Xorg"]

[extra]
thumbnail = "capslock.jpg"
+++

LA DERNIÈRE FOIS QUE J'AI UTILISÉ VOLONTAIREMENT `CAPSLOCK`

Oh, pardon, reprenons.
La dernière fois que j'ai utilisé volontairement `CapsLock`, je devais avoir douze ans et ne savais pas gérer la mise en page de mes titres dans un rapport de stage de troisième.

De l'eau a coulé sous les ponts.
Utiliser un clavier programmable résout le problème sous une première forme: il est possible de supprimer `CapsLock`.

Mais, parfois, il faut revenir à un clavier *normal* comme présentement sur l'ordinateur portable du travail.
En ce cas, tout n'est pas perdu, `xorg` à la rescousse.

<!-- more -->

## La solution

S'il ne s'agit que de fixer temporairement le problème, le temps d'une session sur une machine qui ne nous appartient pas:

```bash
setxkbmap -option caps:escape
```

Cette petite ligne ne va pas juste désactiver `CapsLock` mais lui donner une autre fonction: `Escape`.

## L'automatisation

S'il s'agit de ne plus jamais avoir à se poser la question, il faut simplement ajouter ladite ligne au fichier `~/.xinitrc`:

```bash
setxkbmap -option caps:escape &
exec i3
```

`Escape` remplacera alors l'infâme `CapsLock` à l'ouverture de la session.
