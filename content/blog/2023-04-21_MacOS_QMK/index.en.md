+++
title = "MacOS windows and workspace managers"
description = "macOS UI sucks. Let's try to make it better"

[taxonomies]
about = ["macOS", "QMK", "Shortcuts"]

[extra]
thumbnail = "/blog/2023-04-21_MacOS_QMK/thumbnail.png"
+++

I think I landed my next job.
I'm pretty enthusiast about this future experience, but this is not the topic of this post.

Who says new gig also says new machine.
I've been working on Arch Linux for almost a decade.
But I'll have to switch to macOS now.

And macOS UI sucks so bad I had to find a solution before the onboarding week.
Here list a list of tricks I found to get the most of it using the keyboard.

<!-- more -->

## The issue

I'm used to `i3` on `arch`.
I love them both, but I've to take a separate road for work.

Still, I want to have my *tiling window manager* and my *workspaces*.
And I want them at my fingertips.

macOS has native *workspace* support, which is nice.
So the *tiling* part need some custom configuration.

## The solution

### Tiling

I roamed a bit on the internet before finding `tiles`.
It's a free application and one can install it using `brew`.

The default settings may make sense to a non-vim user.
But the feel pretty awkward to me.
I remove almost every keybinding except for the following ones :

* Left → `Ctrl-Meta-h`
* Down → `Ctrl-Meta-j`
* Up → `Ctrl-Meta-k`
* Right → `Ctrl-Meta-l`
* Full → `Ctrl-Meta-;`

Here, the good old `HJKL` for specific positions and `;` for full screen.
Now I feel comfortable.

`Tiles` is easy to install, runs just fine and gets out of the way.
Definitively worth giving the devs a quarter or two for their awesome work!

### Workspaces

Now comes the *workspaces* configuration.

It's kind of easy (GUI sucks, give me my plain-text settings, please) : `System Settings` → `Keyboard` → `Keyboard Shortcuts` → `Mission Control` → `Mission Control` (Yes, twice...).
There, one can choose its favorite shortcuts.

I picked `Ctrl-N` where `N` is a number from `1` to `0` corresponding to the *workspace* I want to land on.

But wait, I switch between *workspaces* **all the time**.
Why? Well, because I told my `terminal` to stay on *workspace `1`*, `browser` on *workspace `2`* etc.
I keep my head organized this way, so I don't have to rely on the awful `Meta-Shift` workflow.

So, two keys, often...
That's too much.

#### `QMK`

Remember `QMK`?
The firmware my custom keyboards run on.
See where I'm going?

```c
bool process_record_user(uint16_t keycode, keyrecord_t* record) {
    switch (keycode) {
        case (LT(0, KC_Q)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_1)); return false; } break;
        case (LT(0, KC_W)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_2)); return false; } break;
        case (LT(0, KC_E)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_3)); return false; } break;
        case (LT(0, KC_R)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_4)); return false; } break;
        case (LT(0, KC_T)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_5)); return false; } break;
        case (LT(0, KC_Y)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_6)); return false; } break;
        case (LT(0, KC_U)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_7)); return false; } break;
        case (LT(0, KC_I)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_8)); return false; } break;
        case (LT(0, KC_O)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_9)); return false; } break;
        case (LT(0, KC_P)): if (!record->tap.count && record->event.pressed) { tap_code16(C(KC_0)); return false; } break;
    }
    return true;
}
```

What an ugly piece of code!
What does it do?
Simple : if I hold `Q` more than 300ms, the keyboard sends `Ctrl-1`, `W` sends `Ctrl-2` all the way to `P` that sends `Ctrl-0`.
Wow, such handy, much one-key!

I already did this with `i3`, and it's a blessing.
Of course, it only works with my keyboard plugged-in.
While on the laptop I've to use the `Ctrl-N` but since I spend most of my time at my desk, this is OK.

### Bonus

By default, macOS adds a ton of useless visual effects when switching between *workspaces*.
It's slow and serves no functional purpose.

`System Settings` → `Accessibility` → `Display` → `Reduce Motion`.

There, a blur is more than enough.
Well, if you ask me it's still too much.
Hey, Apple Geniuses, please, make fully disabling visual effects possible :pray:

## Conclusion

I'll miss Arch and `i3` dearly.

But I managed to make macOS UI acceptable for now.
Hope `tiles` will keep being updated in the future and Apple won't go the sparkle :sparkle: way too much.

It feels weird to have to fight a computer to make it do simple tasks.
