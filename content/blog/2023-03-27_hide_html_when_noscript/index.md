+++
title = "Cacher du HTML quand JS est inutilisable"
description = "Et oui, car on a pas tous Javascript"

[taxonomies]
sujets = ["html", "javascript", "noscript", "css"]

[extra]
thumbnail = "js.svg"
+++

Comme chacun sait, [tout le monde a `Javascript`](https://www.kryogenix.org/code/browser/everyonehasjs.html) **(non)** actif et fonctionnel durant sa navigation sur internet.

Je ne viens pas du web ou en tous cas pas du *front-end*.
Du coup, je découvre un peu ce monde et je me suis dit que, tant qu'à refaire mon site, autant que les interactions non fonctionnelles soient simplement inaccessibles à l'utilisateur.

Attention, méthode concise et élégante !

<!-- more -->

## HTML: `<noscript>`

La balise `<noscript>`, c'est un peu le *doppelgänger* de la balise `<script>`, son *evil twin*.
Là où `<script>` contient du `Javascript`, `<noscript>` contient du `HTML`.
Et ce `HTML` n'est utilisé que si `Javascript` est inutilisable, peu importe la raison.

Mais bon, encore une fois, je ne viens pas trop du web mais de la programmation impérative avec des boucles et des conditions...
Là, il n'est pas possible de faire un test `if <noscript>`.

## CSS: `display: none`

En `CSS`, `display` permet d'altérer la façon dont sont affichés les différents éléments de la page.
Et choisir `none` revient à cacher totalement le contenu d'une balise.
Pas juste la rendre invisible mais aussi faire en sorte qu'elle ne prenne pas un pixel, même transparent, à l'écran.

## *Now, kiss*

La solution est donc triviale et ne nécessite aucune condition ou logique métier particulière.

### HTML: `class="noscript"`

Indiquer tout ce qui doit disparaître si `Javascript` en attribuant la classe `noscript` aux balises.

Ces balises sont connues à l'avance donc aucun calcul n'est nécessaire côté client pour déterminer s'il est nécessaire d'ajouter ce code.
Tout est fait côté serveur (ou dans mon cas à la génération du site statique).

### CSS: `.noscript { display: none; }`

Là, pas de surprise, c'est la suite logique du paragraphe précédent: toute balise avec la classe `noscript` disparaît de l'écran.

Mais alors voilà... On en revient à la condition.
Parce que c'est bien joli, mais pour l'instant, si le `CSS` est chargé, le `HTML` est caché et ce, peu importe si `Javascript` est utilisable ou non.

### *Best of both worlds*

```html,linenos
 <noscript>
     <link href="/noscript.css" type="text/css" rel="stylesheet" />
 </noscript>
```

Et c'est gagné :tada:

Le code `CSS` qui cache le code `HTML` n'est actif que si `Javascript` est inactif !
Il suffit donc de trois lignes de `HTML`, trois lignes de `CSS` et des classes aux bons endroits.

## Exemple

Ce site fait usage de cette technique.

Dans l'en-tête et le pied de page se trouvent des :sparkles: si `Javascript` est fonctionnel.
Dans ce cas, cliquer sur les :sparkles: ouvre un menu permettant de changer le thème du site.

Mais si `Javascript` n'est pas au rendez-vous, un ensemble de couleurs par défaut est utilisé et le choix n'est plus donné au visiteur :ok_hand:

## Conclusion

Je suppose que ce que j'écris ici correspond au béaba de l'accessibilité.

Cependant, en rencontrant ce problème je m'attendais à une solution capillotractée à base de `media-queries`.
Il n'en est rien et je suis content de l'avoir découverte sur un message de `Stackoverflow` que je n'arrive plus à retrouver désormais.

Ceci étant dit, ce n'est pas le Graal, car certains cas à la marge restent problématiques.
Notamment, il se peut que `Javascript` soit parfaitement fonctionnel sur la machine durant la navigation mais que le code lui-même soit bloqué par un pare-feu en amont.
Je n'ai pas encore cogité à comment gérer cette situation mais ça fera un super article si jamais j'ai besoin de creuser :wave:
