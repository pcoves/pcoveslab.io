+++
title = "(Neo)Vim et Tmux: profiter de la mémoire musculaire"
description = "Utilisation des commandes vim neovim dans bash pour profiter de tmux"

[taxonomies]
sujets = ["IT", "vim", "neovim", "tmux", "bash", "alias"]

[extra]
thumbnail = "neomux.png"
+++

J'utilise `(neo)vim` depuis près d'une décennie.
Et, comme toutes les personnes dans mon cas, j'ai développé une mémoire musculaire permettant de ne plus réfléchir aux commandes tapées.
On pense au but, les doigts bougent, sans passer par la réflexion associant le but et la commande à taper.
Qui dans ce cas n'a jamais tapé `:w<CR>` dans un le navigateur web?

D'accord, c'est parfois handicapant.
Mais dans ce cas, autant tourner ce comportement à notre avantage.
Pourquoi ne pas profiter de cette mémoire musculaire en dehors de l'éditeur de texte?

Quelques *alias* `bash` pour tirer parti de ce long apprentissage.

<!-- more -->

## Ouvrir un *buffer*

Dans `(neo)vim`, ouvrir un *buffer* se fait avec la commande `:edit`, abrégée en `:e`.

```bash
alias :e="nvim"
```

Et hop!
Taper, dans un terminal `:e foo<CR>` va lancer l'éditeur de texte et charger le fichier `foo`.

## Quitter un terminal

On connait tous la blague sur les gens qui n'arrive pas à quitter `(neo)vim`.
Il n'empêche que après des années, on a appris à le faire: `:q<CR>`.

```bash
alias :q="exit"
```

Et hop!.
Taper dans un terminal `:q<CR>` quitte le terminal comme on pourrait quitter `(neo)vim`.

## Intégration de `tmux`.

L'union de `(neo)vim` et `tmux` est devenue aussi naturelle que respirer pour beaucoup de développeurs.
Profitons-en à nouveau.

```bash
alias :vsp="tmux split-window -h -c \"#{pane_current_path}\" --"
alias :sp="tmux split-window -v -c \"#{pane_current_path}\" --"
```

Ces deux alias sont presque identiques.
L'idée derrière est simple: depuis un terminal, lancer une nouvelle commande dans un *split*, vertical ou horizontal respectivement, de `tmux`.

Et hop!
Taper dans un terminal `:vsp htop<CR>` va ouvrir la commande `htop` dans un *split* vertical à côté de l'existant.
