+++
title = "Git push options"
description = "Options par défaut lors d'un git push"

[taxonomies]
sujets = ["Git", "cli", "options", "configuration"]

[extra]
thumbnail = "git.svg"
+++

Ce matin ~~un lapin~~, j'ai pris le temps de faire ce que je me promettais depuis des semaines: retirer mes *alias* `bash` au profit d'*alias* `git` en bonne et due forme.

J'en ai profité pour découvrir d'autres usages de la configuration de `git` via le fichier `.gitconfig` ou `.git/config` tels que les options automatiques lors du *push*.
Mais, il y a un **mais**: il faut parfois retoucher la configuration à la main pour arriver au résultat désiré.

<!-- more -->

## *Alias*

Concernant les *alias*, rien de bien magique ici.
Voici les quelques lignes que j'aurais aimé avoir à mes débuts avec `git`:

### Tirer et nettoyer

```bash
git config --global alias.p "pull --all --prune"
```

Permet de mettre à jour toutes les branches et supprimer localement les branches distantes supprimées.

### `Gitk` en *CLI*

```bash
git config --global alias.k "log --graph --abbrev-commit --decorate --oneline"
```

Affiche dans la console l'*arbre* `git` actuel.

## *Push options*

Pour le compte d'un ~~client~~ partenaire, nous avons mis en place un *pipeline* de *CI/CD*.
Dans ce projet, nous disposons de plusieurs environnements: `zeta`, `alpha`, `beta` etc.
L'environnement par défaut pour les gens avec qui nous collaborons est `alpha` mais pour les développements et tests, mon équipe utilise `zeta`.

Sur `gitlab`, il est possible de passer des variables lors du *push* ainsi: `git push -o ci.variable="foo=bar"`.
Afin de ne pas recouvrir les déploiements des personnes avec lesquelles nous travaillons, il nous faut donc systématiquement ajouter `-o ci.variable="env=zeta"`.
C'est typiquement l'oublie qu'il est facile de faire mais aussi et surtout le genre de chose qu'il devrait être facile à automatiser via une configuration propre au projet.

C'est d'ailleurs rendu possible depuis [ce *commit*](https://github.com/git/git/commit/d8052750c5fdd53cb5a664a18ce9d78dbedb22ae).
Seulement, surprise, tout ne s'est pas passé comme prévu.

```bash
git config --add push.pushOption 'ci.variable="env=zeta"'
```

L'appel précédent ajoute le code suivant dans `.git/config`:

```
[push]
    pushOption = ci.variable=\"env=zeta\"
```

Seulement voilà, les guillements sont échappés.
Cela n'aurait pas été un problème si les *push*s suivants avaient échoué, mais il n'y avait aucune erreur visible alors même que plus aucun *commit* n'était poussé sur le dépôt distant.

En modifiant la configuration comme suit, non seulement les *commit*s sont bien envoyés mais en plus la variable `env` vaut bien `zeta` en l'absence de toute autre option.

```
[push]
    pushOption = ci.variable="env=zeta"
```

## Références

* [*Git push options*](https://git-scm.com/docs/git-push/fr#git-push--oltoptiongt)
