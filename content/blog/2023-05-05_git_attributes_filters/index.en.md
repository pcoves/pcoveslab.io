+++
title = "Share `irssi` config without exposing password"
description = "Leverage git attributes and filter capabilities to share configuration files without exposing sensitive data"

[taxonomies]
about = ["irssi", "git", "filters"]

[extra]
thumbnail = "git.svg"
+++

For some reason, I recently needed to go back on IRC.

My IRC client of choice has always been `irssi`.
It's light, fast, CLI based and easily configurable.

That being said, it has a major drawback : it stores passwords in plain text in the configuration file.
Granted it's not a very sensitive information.
But still, it always discouraged me to add my `~/.irssi` folder to a remote version control system, even a private one.

So, here I am on yet-another-machine, starting yet-another-configuration.
But, this time, I dug a bit into it and found a working solution to this password issue.

<!-- more -->

{% note(class="info") %}
There are multiple ways to identify on IRC networks.
One could use self-signed certificates for example.

The approach I took here is not the only solution to this problem.
I still find it's the one that best suits my personal needs.
{% end %}

## The issue

As stated in the introduction of this post, `irssi` stores password in plain-text along with the rest of its configuration.

A common way to identify oneself on an IRC network is to send a message to a `nickserv` *bot* on connection.

As an example, a configuration block for the *Libera Chat* network would look as follows:

```conf,hl_lines=4
chatnets = {
  libera = {
    type = "IRC";
    autosendcmd = "^msg nickserv identify ${PASSWORD}";
  };
};
```

One can not decently push a file as-is.
The `${PASSWORD}` part needs to be hidden from the outside world.

## The solution

As most software-inclined people nowadays, I use `git` as my main VCS.
And it appears `git` has all the tools I need built-in to bypass the issue.

It's all [here](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#_keyword_expansion) in the documentation.

### `.gitattributes`

The `.gitattributes` file in a `git` repository allows one to require some filters to be run.

```conf
config filter=password
```

Here we go, please, `git` run the `password` filter when needed.

### `.gitconfig`

It's not common to have a `.gitconfig` file inside a `git` repository.
Usually, there is one `~/.gitconfig` (or `~/.config/git/config`) file for machine-wide configuration and one `.git/config` file for repository-specific configuration.
But neither are committed to the remote repository as they both are specific to the current user.

Still, here, it's necessary to not only configure `git` but also share this configuration with anyone with access to the repository.
The approach is to create a `.gitconfig` along with the `.gitattributes` file.

But that's not enough : for security purpose, this file is ignored.
One has to explicitly include it when first cloning the repository.

```bash
git config --local include.path ../.gitconfig
```

{% note(class="warning") %}
Always double-check the `.gitconfig` file's content before including it.
Especially if it comes from a repository you don't fully trust.

It may contain ill-intentioned code and could potentially be a security threat.
{% end %}

### Filter

Now, there is the `.gitattributes` file requiring the `password` filter.
And the `.gitconfig` file which must define the said filter.

```conf
[filter "password"]
    clean = "sed -e 's/identify .*\";/identify <PASSWORD>\";/'"
```

Easy right?
Call `sed`, search for the `identify .*` string and replace it by `identify <PASSWORD>`.

{% note(class="info") %}
Here, the `password` filter is a `clean` filter.

`Clean` filters are applied [**before**](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#filters_b) files are staged.

This means that the original password never reach the staging area and can't be retrieved by carefully looking into `git` internals.
{% end %}

## Conclusion

That's all folks.

My `irssi` configuration can now be pushed to any remote repository without exposing a single password.

Of course, whenever I clone this code, I need to replace the `<PASSWORD>` by hand.
It's a minor inconvenience as it almost never happens.
The said passwords are stored in `pass`, my password manager of choice as already discussed [here](/blog/pass/)[FR] and [here](/blog/password-store/)[FR].
So, it only takes a couple of minutes to be up-and-running with `irssi` :tada:
