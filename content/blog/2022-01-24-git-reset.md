+++
title = "Git reset"
description = "Comment repartir de zéro sur un dépôt existant."

[taxonomies]
sujets = ["CLI", "git", "gitlab"]

[extra]
thumbnail = "gitlab.svg"
+++

Parce que j'oublie systématiquement ces lignes.

En tant normal, je vais créer un dépôt vide, je copie colle les lignes que `gitlab` propose puis je le supprime.
Alors, oui, j'aurais pu garder un dépôt vide, mais où est la plaisir quand je peux ajouter un pense-bête ici?

<!-- more -->

# Start

```bash
git init --initial-branch=main
git remote add origin git@gitlab.com:pcoves/pcoves.gitlab.io.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

# Restart

```
git remote rename origin old-origin
git remote add origin git@gitlab.com:pcoves/pcoves.gitlab.io.git
git push -u origin --all
git push -u origin --tags
```
