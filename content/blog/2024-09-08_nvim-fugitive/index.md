+++
title = "NVim + Fugitive options"
description = "Petit combo de config pour une vie plus douce"

[taxonomies]
sujets = ["vim", "nvim", "git", "fugitive"]

[extra]
thumbnail = "neovim.svg"
+++

Et ben, ça fait un bail que j'ai pas publié ici !
Plus d'un an, ça ne va pas du tout.
J'ai redirigé mes efforts sur de petits _talks_ et j'ai délaissé le _blog_.
C'est mal !

Pour profiter de ce retour (durera-t-il ?), un tout petit article sur `nvim`, `fugitive` et quelques réglages qui vont bien.
Je ne sais pas pourquoi il m'a fallu autant de temps pour corriger ces petites choses qui m'irritaient mais c'est fait et ma vie n'en est que plus douce.

<!-- more -->

## Le problème

J'utilise `nvim`.
(Non, ce n'est pas le problème, vilain troll !)
Et je l'ai configuré pour que, quand je charge un _buffer_, il me replace le curseur là où je l'avais laissé lors de la dernière édition dudit _buffer_.

```lua
vim.api.nvim_create_autocmd("BufReadPost", {
	group = vim.api.nvim_create_augroup("RestoreCursorLocation", { clear = true }),
	callback = function()
		local mark = vim.api.nvim_buf_get_mark(0, '"')
		local lcount = vim.api.nvim_buf_line_count(0)
		if mark[1] > 0 and mark[1] <= lcount then
			pcall(vim.api.nvim_win_set_cursor, 0, mark)
		end
	end,
})
```

> Ce bout de code est une adaptation en `lua` d'un morceau de `vimscript` issu du livre [_Practical Vim_][practicalVim] de _Drew Neil_ que je recommande vivement.

Et, comme beaucoup de développeurs, j'utilise aussi `git` pour versionner mes fichiers.
Pour ce faire, je profite d'un excellent _plugin_ de _Tim Pope_: [`fugitive`][fugitive].

> Maintenant que j'y cogite, mon histoire n'a pas grand chose à voir avec [`fugitive`][fugitive] mais je vais laisser ça ici pour faire un peu de publicité car ce _plugin_ est vraiment formidable.

Bref, je fais ma vie de développeur, tap tap fait le clavier, et vient un moment où il faut `git commit`.
Dans mon _workflow_, ça veut dire lancer [`fugitive`][fugitive] avec `:G<CR>`, choisir les _chunks_ que je souhaite assembler en un _commit_ puis `cc` pour rédiger le _commit message_.

Et là, c'est (parfois) le drame.
Pour peu qu'un _commit_ précédent ait pris plusieurs lignes ou qu'un _rebase_ interactif ait été fait auparavant, `nvim` replace mon curseur _quelque part_.
Hors voilà, un _commit message_ n'a rien à voir avec le précédent ou le suivant, dans ce contexte, le passé ne m'intéresse pas.

## La solution

Elle tient en une toute petite ligne cette solution.
Et il faut la placer dans `after/ftplugin/gitcommit.lua`.

```lua
vim.opt_local.shadafile = "NONE"
```

On indique à `nvim` de ne simplement pas retenir les informations si le _filetype_ est un _commit_ `git`.
C'est bête comme choux, mais au moins chaque commit verra son curseur en première ligne, première colonne et il n'y aura plus jamais de problème lié à la mémoire musculaire de commencer à taper un message au milieu d'un commentaire auto-généré.

## Bonus

Oui parce que bon, tout ce blabla pour une ligne, ça aurait vraiment fait l'auteur qui cherche une excuse pour publier sur son site histoire de retrouver sa motivation hein.

Je ne sais pas vous mais globalement, si je _commit_, c'est pour écrire un message.
Sinon, ben il n'y a pas de _commit_ donc...
Mais pour écrire, il faut passer en mode insertion dans `(n)vim`.

Et bien, toujours dans ce même fichier `after/ftplugin/gitcommit.lua`, j'ai glissé :

```lua
vim.api.nvim_create_autocmd({ "BufEnter" }, {
	group = vim.api.nvim_create_augroup("Git", { clear = true }),
	pattern = "COMMIT_EDITMSG",
	command = "startinsert",
})
```

Ça demande à l'éditeur de passer en mode insertion dès qui entre dans un _buffer_ d'édition de commit.
Et comme vu juste avant, le curseur est forcément bien placé.

## Conclusion

J'ai tourné pendant des années sans le réglage de `shada`.
Souvent induit en erreur par ce comportement, il m'aura fallu bien trop longtemps pour fixer d'une ligne ce désagrément.

Comme d'hab, en espérant que ça serve à d'autres 🤷.

[practicalVim]: https://pragprog.com/titles/dnvim2/practical-vim-second-edition/
[fugitive]: https://github.com/tpope/vim-fugitive
