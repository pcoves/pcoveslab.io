+++
title = "Git Worktrees"
description = "Git Worktrees: how-to, pros and cons"

draft = false

[taxonomies]
sujets = ["IT", "git", "worktrees", "submodules"]

[extra]
thumbnail = "git.svg"
+++

Travailler avec `git`, c'est bien.
Travailler avec des sous-modules, c'est bien aussi, mais ça peut devenir compliqué.
Travailler avec des cascades de sous-modules, ça peut vite devenir l'enfer.

Lorsque l'on change de branche, que l'on ajoute ou supprime des sous-modules avant de revenir à une autre branche, `git` ne sait pas toujours quels fichiers conserver ou supprimer.
Une solution est d'utiliser les `git` [*worktrees*][worktree] afin de séparer clairement les branches et les dossiers qui les contiennent.

Cet article présente la marche à suivre pour ne plus mélanger les branches et leurs sous-modules ainsi que l'intégration de cette fonctionnalité dans `neovim`.

<!-- more -->

> L'usage des [*worktrees*][worktree] prend tout son sens lorsque des sous-modules sont en jeu.
> Dans le cas d'un dépôt simple, je ne suis pas certain qu'il y ait un intérêt quelconque à les utiliser.

# Git

## Clone

La première étape pour commencer à travailler sur un projet est bien sûr de récupérer le code source.
Et c'est là qu'apparaît la première modification du *workflow* habituel: il faut utiliser l'option `--bare` lors du `clone`.

### Avant

```bash,linenos,hl_lines=1
❯ git clone git@gitlab.com:pcoves/pcoves.gitlab.io.git
❯ ls -lh pcoves.gitlab.io/
# Permissions Size User   Date Modified Name
# .rw-r--r--  3.0k pcoves 29 Sep 14:23  config.toml
# drwxr-xr-x     - pcoves 29 Sep 14:22  content
# drwxr-xr-x     - pcoves 29 Sep 14:23  static
# drwxr-xr-x     - pcoves 29 Sep 14:23  themes
❯ ls -lh pcoves.gitlab.io/.git
# Permissions Size User   Date Modified Name
# drwxr-xr-x     - pcoves 29 Sep 14:26  branches
# .rw-r--r--   337 pcoves 29 Sep 14:26  config
# .rw-r--r--    73 pcoves 29 Sep 14:26  description
# .rw-r--r--    29 pcoves 29 Sep 14:26  HEAD
# drwxr-xr-x     - pcoves 29 Sep 14:26  hooks
# .rw-r--r--  5.3k pcoves 29 Sep 14:26  index
# drwxr-xr-x     - pcoves 29 Sep 14:26  info
# drwxr-xr-x     - pcoves 29 Sep 14:26  logs
# drwxr-xr-x     - pcoves 29 Sep 14:26  objects
# .rw-r--r--   410 pcoves 29 Sep 14:26  packed-refs
# drwxr-xr-x     - pcoves 29 Sep 14:26  refs
```

### Après

```bash,linenos,hl_lines=1
❯ git clone --bare git@gitlab.com:pcoves/pcoves.gitlab.io.git
❯ ls -lh pcoves.gitlab.io.git/
# Permissions Size User   Date Modified Name
# drwxr-xr-x     - pcoves 29 Sep 14:25  branches
# .rw-r--r--   134 pcoves 29 Sep 14:25  config
# .rw-r--r--    73 pcoves 29 Sep 14:25  description
# .rw-r--r--    21 pcoves 29 Sep 14:25  HEAD
# drwxr-xr-x     - pcoves 29 Sep 14:25  hooks
# drwxr-xr-x     - pcoves 29 Sep 14:25  info
# drwxr-xr-x     - pcoves 29 Sep 14:25  objects
# .rw-r--r--   365 pcoves 29 Sep 14:25  packed-refs
# drwxr-xr-x     - pcoves 29 Sep 14:25  refs
```

L'option `--bare` permet de ne clone que les fichiers nécessaires au bon fonctionnement du dépôt `git` mais pas le code qu'il manipule.

## Worktree add

Il est désormais nécessaire d'ajouter un [*worktree*][worktree], c'est-à-dire de récupérer le code d'une des branches comme on aurait pu l'avoir "avant", sans l'option `--bare`.

```bash,linenos,hl_lines=2 13
❯ cd pcoves.gitlab.io.git/
❯ git worktree add main main
# Preparing worktree (checking out 'main')
# HEAD is now at 5e246a1 Merge branch 'main' of gitlab.com:pcoves/pcoves.gitlab.io
❯ ls -lh
# Permissions Size User   Date Modified Name
# drwxr-xr-x     - pcoves 29 Sep 14:33  branches
# .rw-r--r--   134 pcoves 29 Sep 14:33  config
# .rw-r--r--    73 pcoves 29 Sep 14:33  description
# .rw-r--r--    21 pcoves 29 Sep 14:33  HEAD
# drwxr-xr-x     - pcoves 29 Sep 14:33  hooks
# drwxr-xr-x     - pcoves 29 Sep 14:33  info
# drwxr-xr-x     - pcoves 29 Sep 14:33  main
# drwxr-xr-x     - pcoves 29 Sep 14:33  objects
# .rw-r--r--   365 pcoves 29 Sep 14:33  packed-refs
# drwxr-xr-x     - pcoves 29 Sep 14:33  refs
# drwxr-xr-x     - pcoves 29 Sep 14:33  worktrees
```

L'ajout d'un *woorktree*, ici suivant la branche `main` fait apparaître un répertoire éponyme contenant le code du dépôt.

```bash,linenos
❯ ls -lh main/
# Permissions Size User   Date Modified Name
# .rw-r--r--  3.4k pcoves 29 Sep 14:33  config.toml
# drwxr-xr-x     - pcoves 29 Sep 14:33  content
# drwxr-xr-x     - pcoves 29 Sep 14:33  sass
# drwxr-xr-x     - pcoves 29 Sep 14:33  static
# drwxr-xr-x     - pcoves 29 Sep 14:33  templates
```

### Add some more

Il est possible d'ajouter autant de [*worktrees*][worktree] que désiré, en parallèle les uns des autres.

```bash,linenos
❯ git worktree add feat/phoenyx/ feat/phoenyx
# Preparing worktree (checking out 'feat/phoenyx')
# HEAD is now at 5688986 Add another pass post
❯ l feat/phoenyx/
# Permissions Size User   Date Modified Name
# .rw-r--r--  3.0k pcoves 29 Sep 14:41  config.toml
# drwxr-xr-x     - pcoves 29 Sep 14:41  content
# drwxr-xr-x     - pcoves 29 Sep 14:41  static
# drwxr-xr-x     - pcoves 29 Sep 14:41  themes
```

Lors du premier `git worktree add main`, `git` a récupéré la branche `main` (branche par défaut) dans le dossier `main` (argument de la commande) `git worktree add`.
Dans le cas présent, j'ai mis deux fois `feat/phoenyx`: le premier est le *path*, le second est la branche que je veux récupérer dans mon [*worktree*][worktree].

```bash,linenos,hl_lines=6 10
❯ ls -lh
# Permissions Size User   Date Modified Name
# drwxr-xr-x     - pcoves 29 Sep 14:33  branches
# .rw-r--r--   134 pcoves 29 Sep 14:33  config
# .rw-r--r--    73 pcoves 29 Sep 14:33  description
# drwxr-xr-x     - pcoves 29 Sep 14:41  feat
# .rw-r--r--    21 pcoves 29 Sep 14:33  HEAD
# drwxr-xr-x     - pcoves 29 Sep 14:33  hooks
# drwxr-xr-x     - pcoves 29 Sep 14:33  info
# drwxr-xr-x     - pcoves 29 Sep 14:33  main
# drwxr-xr-x     - pcoves 29 Sep 14:33  objects
# .rw-r--r--   365 pcoves 29 Sep 14:33  packed-refs
# drwxr-xr-x     - pcoves 29 Sep 14:33  refs
# drwxr-xr-x     - pcoves 29 Sep 14:41  worktrees
```

Au final, je me retrouve avec **un seul** dépôt `git` mais bien **plusieurs** branches en parallèle.
Il est désormais possible de développer dans l'une ou l'autre et les *commits* qui y seront effectués n'affecteront pas les autres [*worktrees*][worktree].

## Worktree remove

Une fois le travail effectué dans un [*worktree*][worktree], il n'est pas nécessaire de le conserver.

```bash,linenos,hl_lines=11
❯ git worktree remove feat/phoenyx
❯ ls -lh
# Permissions Size User   Date Modified Name
# drwxr-xr-x     - pcoves 29 Sep 14:33  branches
# .rw-r--r--   134 pcoves 29 Sep 14:33  config
# .rw-r--r--    73 pcoves 29 Sep 14:33  description
# drwxr-xr-x     - pcoves 29 Sep 14:49  feat
# .rw-r--r--    21 pcoves 29 Sep 14:33  HEAD
# drwxr-xr-x     - pcoves 29 Sep 14:33  hooks
# drwxr-xr-x     - pcoves 29 Sep 14:33  info
# drwxr-xr-x     - pcoves 29 Sep 14:33  main
# drwxr-xr-x     - pcoves 29 Sep 14:33  objects
# .rw-r--r--   365 pcoves 29 Sep 14:33  packed-refs
# drwxr-xr-x     - pcoves 29 Sep 14:33  refs
# drwxr-xr-x     - pcoves 29 Sep 14:49  worktrees
```

Seul le [*worktree*][worktree] `main` est toujours en place, `feat/phoenyx` a été supprimé localement.

## Avantages

En introduction de ce billet, je parle des avantages quant aux sous-modules.

`Git`, par défaut, ne clone pas les sous-modules.
Et cela ne change pas lorsque l'on utilise les [*worktrees*][worktree].

Par contre, il est désormais possible, dans un [*worktree*][worktree], d'aller altérer les *sous-modules* (ajout, suppression, mise-à-jour) sans toucher à l'arborescence des autres [*worktrees*][worktree].
De fait, contrairement à l'état quantique dans lequel un dépôt avec sous-modules se trouve suite à un `git switch`, il y a une garantie nouvelle et bienvenue grâce à la séparation entre les différents [*worktrees*][worktree].

# Neovim

Que serait ce genre de fonctionnalité sans son intégration dans l'éditeur de code?

## Installation

L'ajout du *plugin* `git-worktree.nvim` de [ThePrimeagen](https://github.com/ThePrimeagen) via [`packer`](https://github.com/wbthomason/packer.nvim) est extrêmement simple:

```lua,linenos
use {
    "ThePrimeagen/git-worktree.nvim",
    as = "worktree",
    config = function()
        require("git-worktree").setup()
    end
}
```

## Intégration

```lua,linenos,hl_lines=10-13
use {
    "nvim-telescope/telescope.nvim",
    as = "telescope",
    requires = "plenary",
    config = function()
        vim.keymap.set("n", "<Leader>|", ":Telescope git_files<CR>")
        vim.keymap.set("n", "<Leader><Leader>", ":Telescope buffers<CR>")
        vim.keymap.set("n", "<Leader>g", ":Telescope live_grep<CR>")

        local telescope = require("telescope")
        telescope.load_extension("git_worktree")
        vim.keymap.set("n", "<Leader>w", function() telescope.extensions.git_worktree.git_worktrees() end)
        vim.keymap.set("n", "<Leader>W", function() telescope.extensions.git_worktree.create_git_worktree() end)

        telescope.load_extension('harpoon')
        vim.keymap.set("n", "<Leader>:", function() telescope.extensions.harpoon.marks() end)
    end
}
```

Désormais, `<Leader>w` permet de naviguer d'un [*worktree*][worktree] à l'autre.
C'est aussi dans cette interface qu'un `Ctrl-d` supprime un *woorktree*.

De plus, `<Leader>W` permet de créer un nouveau [*worktree*][worktree] depuis la branche courante.

# Conclusion

Comme dit en préambule, cette fonctionnalité n'est pas la solution à tous les problèmes liés à l'usage des sous-modules.

Cependant, je trouve que j'ai largement gagné en efficacité grâce à elle dans les dépôts où beaucoup de développements ont lieu en parallèle et contenant des sous-modules, car je n'ai pas à gérer les problèmes liés aux changements de branche "sur place".

[worktree]: https://git-scm.com/docs/git-worktree
