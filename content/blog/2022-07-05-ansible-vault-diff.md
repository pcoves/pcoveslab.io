+++
title = "Ansible-Vault et Git diff"
description = "Diff local de fichiers chiffrés avec ansible-vault"

[taxonomies]
sujets = ["IT", "Ansible", "Vault", "Git", "Diff", "CLI"]

[extra]
thumbnail = "git.svg"
+++

Ce matin, un lapin, a voulu merger;
C'était un lapin, qui jouait à `git`.

Je m'égare, je m'égare.
Ce matin, un collègue chargé d'une revue de code a déclaré ne pas pouvoir vérifier le *diff* entre deux versions d'un fichier chiffré avec `ansible-vault`.

D'un côté, il a raison, ce n'est pas possible dans l'interface de `gitlab`.
Pour se faire il faut nécéssairement avoir le mot-de-passe pour déchiffrer les versions avant et après modifications pour les comparer.
De l'autre, localement, c'est tout à fait possible.
Voici un petit script qui simplifie la vie dans ce cas de figure.

<!-- more -->

```bash
#!/bin/bash

function Help() {
    echo
    echo "Diff for ansible-vault encrypted files"
    echo
    echo "$(basename $0) [-c commit] [-f encrypted-file] [-p vault-password-file]"
    echo
    echo "Options:"
    echo "  -c commit"
    echo "  -f encrypted-file"
    echo "  -p vault-password-file"
    exit 0
}

while getopts ":hc:f:p:" option; do
    case $option in
        \?)
            echo Invalid -$OPTARG option
            Help
            exit;;
        h)
            Help
            exit;;
        c) commit=$OPTARG;;
        f) encrypted_file=$OPTARG;;
        p) vault_password_file=$OPTARG;;
    esac
done

[[ -z "${commit:+x}" ]]              && echo "Missing -c commit argument"              && Help;
[[ -z "${encrypted_file:+x}" ]]      && echo "Missing -f encrypted-file argument"      && Help;
[[ -z "${vault_password_file:+x}" ]] && echo "Missing -p vault-password-file argument" && Help;

diff -u \
    <(ansible-vault view --vault-password-file="${vault_password_file}" <(git show "${commit}^:${encrypted_file}")) \
    <(ansible-vault view --vault-password-file="${vault_password_file}" <(git show "${commit}:${encrypted_file}"))
```

Rien de magique ici:
1. Recueillir les informations via les arguments en ligne de commande,
2. Déchiffrer les deux versions via `ansible-vault`,
3. Les passer à `diff` pour visualisation.

Plus d'excuse pour les revues !
