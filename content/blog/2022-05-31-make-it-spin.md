+++
title = "Make it spin - A GIF maker"
description = "Script de génération de GIFs rotatifs à partir d'une seule image"

[taxonomies]
sujets = ["Code", "gif", "scripg", "shell"]

[extra]
thumbnail = "autruche.gif"
+++

Je suis bien en mal de me souvenir ce qui a déclenché la rédaction de ce script que j'avais oublié depuis bien longtemps.
Mais il a été écrit et il vient de resservir pour une bêtise entre collègues.

*Behold*, le script qui prend une simple image et la fait tourner.

<!-- more -->

Ce script est un simple *wrapper* autour de `convert` qui vient avec le *package* `imagemagick`.
Il faut donc avoir ce dernier installé sur sa machine pour que le code suivant fonctionne.

L'idée est assez triviale:
1. Prendre une image est en générer `N` versions tournées de `i * 360 / N` degrés.
2. Assembler toutes ces images en un GIF.
3. Nettoyer les fichiers/dossies temporaires.

```bash
#!/bin/bash

function Help() {
    echo
    echo "Turns an image into a rotating GIF of itself"
    echo "Default output is at input's side"
    echo
    echo "gif [option] input"
    echo
    echo "Options :"
    echo "  -d      Delay between each frame ; Default = 10"
    echo "  -n      Number of frames (approx); Default = 36"
    echo "  -o      Output file              ; Default = input.gif"
    echo "  -v      Verbose"
    echo
}

while getopts ":hd:n:o:v" option;
do
    case $option in
        h)
            Help
            exit;;
        d)
            if ! [[ "$OPTARG" =~ ^[0-9]+$ ]]
            then
                echo "Delay argument must be an integer"
                Help
                exit
            fi

            delay=$OPTARG;;
        n)
            if ! [[ "$OPTARG" =~ ^[0-9]+$ ]]
            then
                echo "Number argument must be an integer"
                Help
                exit
            fi

            number=$(( $OPTARG ));;
        o)
            fulloutput=$OPTARG

            output=$(basename "$fulloutput")
            extension="${output##*.}"

            if [ $extension -ne "gif" ]; then
                fulloutput="$fulloutput.gif"
            fi;;
        v)
            verbose=1;;
        \?)
            echo "Invalid -$OPTARG option"
            Help
            exit;;
        :)
            echo "Missing argument for -$OPTARG"
            Help
            exit;;
    esac
done

number=${number:-"35"}
delay=${delay:-"10"}

fullinput=${@:$OPTIND:1}

if [ -v "$fullinput" ]; then
    echo "Missing file argument"
    Help
    exit
fi

fulloutput=${fulloutput:-"$fullinput.gif"}

input=$(basename "$fullinput")
extension="${input##*.}"

tmp=`mktemp -d`

if [ $verbose ]; then
    echo
    echo "Full path     : $fullinput"
    echo "File          : $input"
    echo "Extenction    : $extension"
    echo "Tmp directory : $tmp"
    echo "Output        : $fulloutput"
    echo
fi

for i in $(seq 0 $number)
do
    rotation=$(( 360 / $number ))
    angle=$(( $rotation * $i ))
    string="000$angle"
    convert -background transparent -distort SRT $angle "$fullinput" "$tmp/${string:(-4)}.$extension"
done

convert -delay $delay -loop 0 -dispose previous "$tmp/*" "$fulloutput"

rm -rf $tmp
```
