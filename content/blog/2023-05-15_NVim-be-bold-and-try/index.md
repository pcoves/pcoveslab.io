+++
title = "N'ayez pas peur de votre éditeur de texte"
description = "Vanilla, c'est bien, mais il ne faut pas avoir peur de dépasser cet état de fait"

updated = 2023-05-16

[taxonomies]
sujets = ["Neovim", "nvim", "vim", "configuration", "dotfiles", "stow", "NVIM_APPNAME"]

[extra]
thumbnail = "neovim.svg"
+++

Il y a quelques jours, un copain des [TupperVim][tuppervim] a publié un [super article][article] sur les petites choses qu'il aurait aimé savoir au moment de commencer à utiliser [`neovim`][neovim].

Peu de temps après, il nous a rapporté avoir discuté avec d'autres utilisateurs et être étonné que nombre d'entre eux utilisent leur éditeur presque sans configuration.

Personnellement, je n'ai rien contre l'usage *nu*.
D'ailleurs [ma configuration][dotfiles] est pensée pour ne remplacer **aucune** fonctionnalité de base par des *plugins*.
L'idée étant que, lorsque j'utilise [`vim`][vim] sur des serveurs distants, il est généralement vierge de toute configuration, que ce soit la mienne ou celle de quelqu'un d'autre.
Je veux donc pouvoir être efficace instantanément sans avoir besoin de réglages particuliers et potentiellement longs à mettre en place.

Ceci étant dit, cela ne signifie pas que je n'apprécie pas les avantages que proposent les plugins ou même un peu de configuration sans faire appel à du code externe.
Le but de cet article est donc de couvrir ma méthode pour récupérer et faire évoluer ma configuration ainsi que mon approche pour tester les configurations d'autrui sans altérer la mienne.

<!-- more -->

## Ma configuration

C'est la mienne. À moi.

J'ai passé du temps dessus.
Assez de temps pour me dire qu'il peut valoir le coup de la partager.
D'ailleurs, elle est [là][dotfiles].

Et je pense que la personne avec laquelle je partage le plus ma configuration n'est autre que moi.
Mais le moi de l'autre ordinateur : le *professionnel*, le *personnel*, le serveur auquel je me connecte fréquemment...
C'est toujours moi mais jamais au même endroit.

### Comment faire simple ?

Hé bien, tout comme le copain, il y a quelque chose que j'aurais aimé connaître à mes débuts : [`stow`][stow].
Stow est un outil permettant d'automatiser la gestion de liens symboliques.
Et c'est l'outil parfait pour gérer la configuration de logiciels.

Prenons, à tout hasard, [ma configuration][dotfiles].
Et tout particulièrement son `README`:

```bash
git clone git@gitlab.com:pcoves/nvim.git
stow --target ~ nvim
```

Ces deux petites lignes, que font-elles ?

1. Cloner ma configuration. Peu importe où sur une machine.
2. Appeler `stow` avec pour cible `~` sur le dossier fraîchement récupéré.

Le dossier en question contient l'arborescence suivante :

```
/wherever/you/want/nvim
├── .config
│   └── nvim
├── .git
├── .gitignore
├── .stow-local-ignore
└── README.md
```

* Un dossier `.config` contenant la configuration `nvim`. C'est la partie importante.
* La configuration `.git`. Après tout, c'est un dépôt qui vient d'être cloné.
* La configuration `stow`. Je reviens dessus sans une minute.
* Un `README.md` parce qu'un dépôt sans documentation ne devrait pas exister.

La petite ligne vue tout à l'heure `stow --target ~ nvim` va prendre le contenu de ce dossier et créer des liens symboliques vers chaque élément depuis la racine `~`.
Sauf que les liens vers `git` ou la documentation ne sont d'aucun intérêt à cet endroit; C'est pourquoi `stow` est configuré pour les ignorer grâce au fichier `.stow-local-ignore`.

Au final donc, seul le dossier `.config` va être pris en compte.
En conséquence, `~/.config/nvim` pointera sur `/wherever/you/want/nvim/.config/nvim`.

Deux lignes en console et hop, j'ai ma configuration prête à l'emploi.
Que demander de plus ?

### Pourquoi faire simple ?

Hé bien, tant qu'on y est, s'il est simple de la récupérer, le mieux serait qu'il soit simple de la modifier.

Ça tombe bien, c'est un dépôt `git`.
Et qui dit `git` dit une branche et c'est plié.

J'ai une paire de *bindings* pour aller plus vite.
On peut les trouver [ici](https://gitlab.com/pcoves/nvim/-/blob/main/.config/nvim/after/plugin/config.lua)

* `<Leader><` ouvre la configuration générale. Et en cas de modification sur disque, elle est rechargée automatiquement.
* `<Leader>>` ouvre la configuration des plugins.

Et voilà, c'est simple à récupérer, simple à éditer, simple à propager.
Ce n'est pas automatique, il faut penser à tirer les modifications en arrivant sur une autre machine.
Mais comparé à mes débuts où mes configurations étaient indépendantes sur chaque machine et qu'il m'arrivait fréquemment d'en perdre tout ou une partie, promis, c'est bien mieux.

## La configuration **des autres**

Ben oui quoi, je ne suis pas seul à utiliser `neovim`.
Et **les autres**, même si c'est parfois un peu l'enfer, peuvent avoir des idées pas déconnantes.
Alors bon, autant que je puisse me servir.

### Avant (c'était pas mieux)

Avant, tout reposait encore sur `stow`.

```bash
stow --delete --target ~ nvim
```

Je commençais par supprimer les liens symboliques vers ma configuration.
Puis je faisais la même chose qu'avec [ma configuration](#ma-configuration) mais avec celle des copains.

```bash
git clone https://github.com/manteapi/nvim copain
stow --target ~ copain
```

Et, hop, je me retrouve avec la configuration de quelqu'un d'autre.
Et si jamais mon lectorat est curieux, je conseille d'aller jeter un œil à [celle de l'exemple](https://github.com/manteapi/nvim) qui regorge de petits *plugins* rendant l'expérience bien plus douce que la mienne qui reste très *brute* en comparaison.
Et, c'est bien ça le but de la démarche : aller voir en quelques lignes comment c'est ailleurs, piquer les bonnes idées et les intégrer.

Sauf que voilà, c'est un peu lassant de devoir faire appel à `stow` dans un sens et puis dans l'autre pour chaque test.
Heureusement, les développeurs de `neovim` sont des gens qui pensent à leurs utilisateurs.

### Maintenant (c'est mieux que si c'était moins bien)

Depuis [`neovim` 0.9](https://github.com/neovim/neovim/releases/tag/v0.9.0), il y a une petite fonctionnalité sympathique : `NVIM_APPNAME`.

La variable d'environnement `NVIM_APPNAME`, si définie, peut pointer sur une configuration spécifique à partir du dossier `~/.config`.
Si elle n'est pas définie, c'est `~/.config/nvim` qui est utilisée bien sûr.

Donc, sans plus faire appel à `stow` désormais, si quelqu'un veut tester ma configuration :

```bash
git clone https://gitlab.com/pcoves/nvim.git ~/.config/pcoves
NVIM_APPNAME=pcoves/.config/nvim nvim
```

Et **paf !**, `neovim` se lance en ignorant totalement `~/.config/nvim`.
L'éditeur ne va pas non plus remplacer `~/.local/share/nvim` ni `~/.local/state/nvim` où sont écrits les informations utiles à `neovim` en cours d'utilisation.
Il va créer `~/.local/share/pcoves` et `~/.local/state/pcoves` et garder les deux configurations hermétiquement séparées.

## Conclusion

Voilà donc ce que j'aurais aimé savoir au tout début afin d'avancer plus vite vers un usage unifier de `vim` (à l'époque).

1. Partager et modifier facilement ma configuration.
2. Copier et tester facilement d'autres configurations.

D'autres idées brillantes à venir sur le même sujet dans d'autres articles ?
En attendant, ne pas hésiter à dire ~~ce que vous pensez~~ du bien de mes billets ou me poser des questions sur [mastodon](https://mamot.fr/@PacoVelobs) :wave:

Et pour finir, un grand merci à Fabrice alias Chouhartem qui a publié [l'article initial][article] et lancé la conversation autour de ces outils :+1:

[article]: https://blog.epheme.re/fr/programmes/nvim.html
[dotfiles]: https://gitlab.com/pcoves/nvim
[neovim]: https://neovim.org
[tuppervim]: https://tuppervim.org
[vim]: https://www.vim.org
[stow]: https://www.gnu.org/software/stow/
